pragma solidity ^0.4.4;

contract Splitter {

  event LogSplitRequested(address bob, address carol, uint amount);

  address public alice;
  address public bob;
  address public carol;

  modifier onlyAlice() {
    require(msg.sender == alice);
    _;
  }

  function Splitter(address b, address c)
  {
    alice = msg.sender;
    bob = b;
    carol = c;
  }

  function()
    onlyAlice()
    public
    payable
  {
    require(msg.value > 0);
    bob.transfer(msg.value/2);               // Yes,
    carol.transfer(msg.value-(msg.value/2)); // Bob will have less than Carol, but too bad Bob.
                                             // You ain't gonna refuse money are ya?
    LogSplitRequested(bob, carol, msg.value);
  }

  function kill() 
    onlyAlice()
    public 
  {
    selfdestruct(alice);
  }
}
