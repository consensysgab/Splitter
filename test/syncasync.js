var Splitter = artifacts.require("./Splitter.sol");

//const Promise = require("bluebird");
//Promise.promisifyAll(web3.eth, { suffix: "Promise" });
//Promise.promisifyAll(web3.version, { suffix: "Promise" });

// Don't need bluebird, this promisifies all functions.
Extensions = require("../utils/extensions.js");
Extensions.init(web3, assert);

contract('Splitter', function(accounts) {
  var contract;
  var alice;
  var bob;
  var carol;

  before("should prepare accounts", function () {
    assert.isAbove(accounts.length, 2, "should have at least 3 accounts");
    alice = accounts[0];
    bob = accounts[1];
    carol = accounts[2];
    return Extensions.makeSureAreUnlocked([ alice ])
      .then(() => web3.version.getNodePromise())
      .then(node => isTestRPC = node.indexOf("EthereumJS TestRPC") > -1)
      .then(() => web3.eth.getBalancePromise(alice))
      .then(balance => assert.isTrue(
        web3.toWei(web3.toBigNumber(10), "finney").lessThan(balance),
        "should have at least 10 finney, not " + web3.fromWei(balance, "finney")));
  });

  beforeEach(function() {
    return Splitter.new(bob, carol, {from: alice})
    .then(function(instance){
      contract = instance;
    });
  });

  it("should split 1", function() {
    var bobPreBalance, carolPreBalance, bobPostBalance, carolPostBalance;
    return web3.eth.getBalancePromise(bob)
    .then(function(b) {
      bobPreBalance = b;
      return web3.eth.getBalancePromise(carol);
    })
    .then(function(b) {
      carolPreBalance = b;
      return web3.eth.sendTransactionPromise({to: contract.address, from: alice, value:1});
    })
    .then(function(tx) {
      return web3.eth.getTransactionReceiptMined(tx);
    })
    .then(function(receipt) {
      var ev = contract.LogSplitRequested().formatter(receipt.logs[0]);
      assert.equal(bob, ev.args.bob, "Bob isn't bob.");
      assert.equal(carol, ev.args.carol, "Carol isn't Carol.");
      assert.equal(1, ev.args.amount.toString(10), "Amount isn't 1.");
      return web3.eth.getBalancePromise(bob);
    })
    .then(function(b) {
      bobPostBalance = b;
      return web3.eth.getBalancePromise(carol);
    })
    .then(function(b) {
      carolPostBalance = b;
      assert.equal(bobPreBalance.toString(10), bobPostBalance.toString(10), "Bob won't be happy");
      assert.equal(carolPreBalance.plus(1).toString(10), carolPostBalance.toString(10), "Carol won't be happy");
      console.log("T1 done");
    });
  });

});
