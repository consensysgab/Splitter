var Splitter = artifacts.require("./Splitter.sol");

contract('Splitter', function(accounts) {
  var contract;
  var alice = accounts[0];
  var bob   = accounts[1];
  var carol = accounts[2];

  beforeEach(function() {
    return Splitter.new(bob, carol, {from: alice})
    .then(function(instance){
      contract = instance;
    });
  });

  it("should be owned by alice", function() {
    return contract.alice({from: alice})
    .then(function(o) {
      assert.strictEqual(o, alice, "Not owned by right person");
    });
  });

/*  If we can catch the error, validating an invalid opcode.
  it("shouldn't split 0", function() {
    var bobPreBalance = web3.eth.getBalance(bob);
    var carolPreBalance = web3.eth.getBalance(carol);
    return contract.sendTransaction({from: alice, value:0})
    .then(function(tx) {
      var bobPostBalance = web3.eth.getBalance(bob);
      var carolPostBalance = web3.eth.getBalance(carol);
      assert.equal(bobPreBalance.toString(10), bobPostBalance.toString(10), "Bob won't be happy");
      assert.equal(carolPreBalance.toString(10), carolPostBalance.toString(10), "Carol won't be happy");
    });
  });
*/

  it("should split 1", function() {
    var bobPreBalance = web3.eth.getBalance(bob);
    var carolPreBalance = web3.eth.getBalance(carol);
    return contract.sendTransaction({from: alice, value:1})
    .then(function(tx) {
      var bobPostBalance = web3.eth.getBalance(bob);
      var carolPostBalance = web3.eth.getBalance(carol);
      assert.equal(bobPreBalance.toString(10), bobPostBalance.toString(10), "Bob won't be happy");
      assert.equal(carolPreBalance.plus(1).toString(10), carolPostBalance.toString(10), "Carol won't be happy");
    });
  });

  it("should split 9", function() {
    var bobPreBalance = web3.eth.getBalance(bob);
    var carolPreBalance = web3.eth.getBalance(carol);
    return contract.sendTransaction({from: alice, value:9})
    .then(function(tx) {
      var bobPostBalance = web3.eth.getBalance(bob);
      var carolPostBalance = web3.eth.getBalance(carol);
      assert.equal(bobPreBalance.plus(4).toString(10), bobPostBalance.toString(10), "Bob won't be happy");
      assert.equal(carolPreBalance.plus(5).toString(10), carolPostBalance.toString(10), "Carol won't be happy");
    });
  });

  it("should split 10", function() {
    var bobPreBalance = web3.eth.getBalance(bob);
    var carolPreBalance = web3.eth.getBalance(carol);
    return contract.sendTransaction({from: alice, value:10})
    .then(function(tx) {
      var bobPostBalance = web3.eth.getBalance(bob);
      var carolPostBalance = web3.eth.getBalance(carol);
      assert.equal(bobPreBalance.plus(5).toString(10), bobPostBalance.toString(10), "Bob won't be happy");
      assert.equal(carolPreBalance.plus(5).toString(10), carolPostBalance.toString(10), "Carol won't be happy");
    });
  });

});
